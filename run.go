package main

import (
	"net/http"
	"log"
	"path/filepath"
	"go/build"
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"math/rand"
	"strconv"
	"github.com/wskinner/hijackme/hijackme"
	"strings"
)

type Users struct {
	ActiveSessions map[int64]string	// sessionId -> username
}

var users = Users{ActiveSessions: make(map[int64]string)}

func defaultAssetPath() string {
	p, err := build.Default.Import("github.com/wskinner/hijackme/hijackme/assets", "", build.FindOnly)
	if err != nil {
		return "."
	}
	return p.Dir
}

func main() {
	println("Starting the server")

	http.HandleFunc("/", splashHandler)
	http.HandleFunc("/home", homeHandler)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/logmein", logmeinHandler)
	http.HandleFunc("/images/", imagesHandler)
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/adduser", adduserHandler)
	http.HandleFunc("/search", searchHandler)
	http.HandleFunc("/users/", usersHandler)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

type payload struct {
	Username string
	Images []string
	SearchResults []string
}

func splashImagePath() string {
	return "/images/caged.jpg"
}

func splashHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles(filepath.Join(defaultAssetPath(), "splash.html"))
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	t.Execute(w, splashImagePath())
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Home page requested")
	cookie, _ := r.Cookie("SESSIONID")
	t, err := template.ParseFiles(filepath.Join(defaultAssetPath(), "home.html"))
	if err != nil {
		log.Printf("Error: %v\n", err)
	}

	payload := payload{Username: ""}
	if cookie != nil {
		sessionid, _ := strconv.ParseInt(cookie.Value, 10, 64)
		log.Printf("sess id: %d\n", sessionid)
		payload.Username = users.ActiveSessions[sessionid]
		log.Printf("username: %s\n", users.ActiveSessions[sessionid])
		payload.Images = imagePathsU(sessionid)
	}
	t.Execute(w, payload)
}

func adduserHandler(w http.ResponseWriter, r *http.Request) {
	uid := r.FormValue("login")
	password := r.FormValue("password")
	u := catlocker.User{uid, password}
	log.Println(u)

	cmp := catlocker.CheckUser(u)
	if !cmp {
		log.Printf("User %v already exists\n", uid)
		http.Redirect(w, r, "/login", 303)
	}

	err := catlocker.AddUser(u)
	if err != nil {
		log.Printf("Error: %v\n", err)
		http.Redirect(w, r, "/login", 303)
	}
	logmeinHandler(w, r)
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Login page requested")
	http.ServeFile(w, r, filepath.Join(defaultAssetPath(), "login.html"))
}

func logmeinHandler(w http.ResponseWriter, r *http.Request) {
	uid := r.FormValue("login")
	password := r.FormValue("password")
	u := catlocker.User{uid, password}
	log.Println(u)
	cmp := catlocker.CheckPassword(u)
	if cmp {
		log.Printf("Logging user %s in", uid)
	} else {
		log.Printf("Password comparison failed for user %s\n", uid)
		http.Redirect(w, r, "/login", 303)
	}
	sessionid := rand.Int63()
	cookie1 := http.Cookie{Name: "USERNAME", Value: uid, Path: "/"}
	cookie2 := http.Cookie{Name: "SESSIONID", Value: strconv.FormatInt(sessionid, 10), Path: "/"}
	r.AddCookie(&cookie1)
	r.AddCookie(&cookie2)
	http.SetCookie(w, &cookie1)
	http.SetCookie(w, &cookie2)

	users.ActiveSessions[sessionid] = uid

	path := filepath.Join(filepath.Join(defaultAssetPath(),"images"), uid)
	if !exists(path) {
		os.Mkdir(path, 0700)
	}

	http.Redirect(w, r, "/home", 303)
}

func imagesHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling image request")
	path := filepath.Join(defaultAssetPath(), r.URL.Path)
	log.Printf("Serving %s\n", path)
	http.ServeFile(w, r, path)
}

func searchHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		log.Printf("Expected POST, but was %s\n", r.Method)
		return
	}


	cookie, _ := r.Cookie("SESSIONID")
	t, err := template.ParseFiles(filepath.Join(defaultAssetPath(), "home.html"))
	if err != nil {
		log.Printf("Error: %v\n", err)
	}

	payload := payload{Username: ""}
	if cookie != nil {
		sessionid, _ := strconv.ParseInt(cookie.Value, 10, 64)
		log.Printf("sess id: %d\n", sessionid)
		payload.Username = users.ActiveSessions[sessionid]
		log.Printf("username: %s\n", users.ActiveSessions[sessionid])
		payload.Images = imagePathsU(sessionid)
	}

	query := r.FormValue("query")
	results := catlocker.SearchUsers(query)

	payload.SearchResults = results
	t.Execute(w, payload)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		log.Println("Ignoring GET")
		return
	}
	log.Println("Handling upload")
	cookie, _ := r.Cookie("SESSIONID")
	sessionid, _ := strconv.ParseInt(cookie.Value, 10, 64)
	uid := users.ActiveSessions[sessionid]

	//parse the multipart form in the request
	err := r.ParseMultipartForm(100000)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//get a ref to the parsed multipart form
	m := r.MultipartForm

	//get the *fileheaders
	log.Println(m)
	log.Println(m.File)
	files := m.File["files"]
	for i, _ := range files {
		//for each fileheader, get a handle to the actual file
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//create destination file making sure the path is writeable.
		path := filepath.Join(filepath.Join(filepath.Join(defaultAssetPath(), "images"), uid), files[i].Filename)
		dst, err := os.Create(path) 
		defer dst.Close()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//copy the uploaded file to the destination file
		if _, err := io.Copy(dst, file); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

	}
	http.Redirect(w, r, "/home", 303)
}

func usersHandler(w http.ResponseWriter, r *http.Request) {
	host := strings.Split(r.URL.Path, "/")
	uid := host[len(host)-1]

	iPaths := imagePathsS(uid)

	t, err := template.ParseFiles(filepath.Join(defaultAssetPath(), "users.html"))
	if err != nil {
		log.Printf("Error: %v\n", err)
	}

	payload := payload{Username: uid, Images: iPaths}
	t.Execute(w, payload)
}

func exists(path string) (bool) {
	_, err := os.Stat(path)
	if err == nil { 
		return true
	} else {
		return false
	}
}

func imagePathsS(uid string) []string {
	path := filepath.Join(filepath.Join(defaultAssetPath(), "images"), uid)
	files, _ := ioutil.ReadDir(path)
	names := make([]string, len(files))
	for _, f := range files {
		names = append(names, "/images/" + uid + "/" + f.Name())
	}
	if len(names) <= 1 {
		return names
	}
	return names[1:]

}

func imagePathsU(sessid int64) []string {
	uid, ok := users.ActiveSessions[sessid]
	if !ok {
		return make([]string, 0)
	}
	return imagePathsS(uid)
}

