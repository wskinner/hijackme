package catlocker

import (
	"fmt"
	"code.google.com/p/gosqlite/sqlite"
	"log"
	"crypto/md5"
	"encoding/hex"
	"os/exec"
	"strings"
)

type User struct {
	Username string
	Password string
}

type Pool struct {
	Conn *sqlite.Conn
}

var p = Pool{}

func getDatabaseConnection() *sqlite.Conn {
	if p.Conn == nil {
		log.Println("Initializing DB connection")
		dbconn, err := sqlite.Open("catlocker.db")
		if err != nil {
			log.Printf("Error: %v\n", err)
			return nil
		}
		p.Conn = dbconn
	}
	return p.Conn
}

// Unsalted MD5 is good enough for this crappy app
func hash(password string) string {
	h := md5.New()
	h.Write([]byte(password))
	return hex.EncodeToString(h.Sum(nil))
}


func AddUser(user User) error {
	conn := getDatabaseConnection()
	log.Printf("Adding user: %v\n", user)
	h := hash(user.Password)
	sql := fmt.Sprintf("INSERT INTO Users(id, username, password_hash) VALUES(NULL, '%v', '%v');", user.Username, h)
	log.Printf("Running query: %v\n", sql)
	err := conn.Exec(sql)
	if err != nil {
		log.Printf("AddUser Error: %v\n", err)
	}
	return err
}

func CheckUser(user User) bool {
	conn := getDatabaseConnection()
	sql := fmt.Sprintf("SELECT username FROM Users WHERE username='%v';", user.Username)
	log.Printf("Running query: %v\n", sql)
	query, err := conn.Prepare(sql)
	if err != nil {
		log.Println(err)
		return false
	}
	err = query.Exec()
	if err != nil {
		log.Println(err)
	}
	if query.Next() {
		return false
	} else {
		return true
	}
}

func CheckPassword(user User) bool {
	conn := getDatabaseConnection()
	h := hash(user.Password)
	sql := fmt.Sprintf("SELECT password_hash FROM Users WHERE username='%v';", user.Username)
	log.Printf("Running query: %v\n", sql)
	query, err := conn.Prepare(sql)
	err = query.Exec()
	if err != nil {
		log.Printf("CheckPassword query Error: %v\n", err)
		return false
	}
	var test string
	if err == nil && query.Next() {
		query.Scan(&test)
		if test != h {
			log.Printf("Got: %s, Expected: %s\n", test, h)
			return false
		}
		log.Printf("Password comparison succeeded\n")
		return true
	} else {
		log.Printf("CheckPassword Error: %v\n", err)
		return false
	}
}

func SearchUsers(query string) (results []string) {
	log.Printf("Query: %v\n", query)
	sql := fmt.Sprintf("SELECT username FROM Users WHERE username LIKE '%v';", query)

	if cmd := exec.Command("sqlite3", "/Users/will/gohome/src/github.com/wskinner/hijackme/catlocker.db", sql); cmd != nil {
		out, err := cmd.Output()
		if err != nil {
			log.Println(err)
		}

		results = strings.Split(string(out), "\n")
		log.Println(results)
	} else {
		log.Println("cmd nil")
	}

	return results
}
